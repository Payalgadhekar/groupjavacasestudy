package org.test.database;

public class User {
	int id,phone;
	String name,password,email,role;
	
	public User(int id, String name, int phone,String password, String email, String role) {
		this.id = id;
		this.phone = phone;
		this.name = name;
		this.password = password;
		this.email = email;
		this.role = role;
	}
	public User() {
	
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return String.format("%-5d%-10s%-55s%-20s%-20s%-5.2f", this.id, this.name, this.password, this.email, this.phone,this.password);
	}

}
