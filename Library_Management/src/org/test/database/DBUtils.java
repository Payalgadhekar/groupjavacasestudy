package org.test.database;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import org.test.book.Book;

public class DBUtils implements Closeable{
	private static Properties properties;
	private Connection connection;
	private PreparedStatement stmtInsert;
	private PreparedStatement stmtSelect;

	static {
		try {
			properties = new Properties();
			FileInputStream inputStream = new FileInputStream("Config.properties");
			properties.load( inputStream );
			Class.forName(properties.getProperty("DRIVER"));
		}catch( Exception cause ) {
			throw new RuntimeException(cause);
		}
	}
	public static Connection getConnection( )throws Exception{
		return DriverManager.getConnection(properties.getProperty("URL"),properties.getProperty("USER"),properties.getProperty("PASSWORD"));
	}
	
	ArrayList<Book> LoadAllBooks() {
		 ArrayList<Book> CurrentBooks = new ArrayList<>();

		try( Connection connection = DBUtils.getConnection();
				Statement statement = connection.createStatement();){
			
			String sql = "SELECT * FROM books";
			try( ResultSet rs = statement.executeQuery(sql)){
				while( rs.next()) {
					Book book = new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getFloat(5));
					CurrentBooks.add(book);
					//System.out.println(book.toString());
				}
			}
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
		return CurrentBooks;
		
		
	}
	 void AddNewBook(int book_id, String subject_name, String book_name,String author_name, int price) {

		 try( Connection connection = DBUtils.getConnection();
					Statement statement = connection.createStatement();){
				
			 statement.executeUpdate("Insert into books (book_id,subject_name ,book_name,author_name,price) values('" + book_id + "','" + subject_name + "','" + book_name + "','" + author_name + "','" + price + "')");

	        } catch (Exception e) {
	            System.out.println(e);

	        }

	    }

	    boolean DeleteABook(int book_id) {

	        boolean flag=false;
	      
	        	try( Connection connection = DBUtils.getConnection();
	 					Statement statement = connection.createStatement();){
	 		
	        		statement.executeUpdate("Delete From books  Where book_id='" + book_id + "'");
	            flag=true;

	        } catch (Exception e) {
	            System.out.println(e);

	        }
	        
	      
	      return flag;
	    }

	    ArrayList<Book> SearchBookbyTitle(String input) {

	        ArrayList<Book> BooksList = new ArrayList<>();

	        try( Connection connection = DBUtils.getConnection();
 					Statement statement = connection.createStatement();){
 		
	            ResultSet rs = statement.executeQuery("select book_id from books where subject_name='" + input + "'");
	            while (rs.next()) {

	                int book_id = rs.getInt(1);
	                //Book NewBook = GetaBookbyId(book_id);
	               // BooksList.add(NewBook);

	            }

	        } catch (Exception e) {
	            System.out.println(e);
	        }

	        return BooksList;

	    }

		@Override
		public void close() throws IOException {
			// TODO Auto-generated method stub
			
				try {
					this.stmtInsert.close();
					this.stmtSelect.close();
					this.connection.close();
				} catch (SQLException cause) {
					throw new IOException(cause);
				}
		}

		public DBUtils() throws Exception {
			this.connection = DBUtils.getConnection();
			this.stmtInsert = this.connection.prepareStatement("INSERT INTO user VALUES(?,?,?,?,?,?)");
			this.stmtSelect = this.connection.prepareStatement("SELECT * FROM user");
		}
		//C
		public int insert(User user) throws Exception{
			this.stmtInsert.setInt(1, user.getId());
			this.stmtInsert.setString(2, user.getName());
			this.stmtInsert.setInt(3, user.getPhone());
			this.stmtInsert.setString(4, user.getEmail());
			this.stmtInsert.setString(5, user.getRole());
			this.stmtInsert.setString(6, user.getPassword());
			return this.stmtInsert.executeUpdate( );
		}

}
