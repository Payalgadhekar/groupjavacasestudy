package org.test.main;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import org.test.book.Book;
import org.test.common.Preferances;
import org.test.database.DBUtils;
import org.test.database.User;
import org.test.librarian.librarianMenu;
import org.test.owner.OwnerMenu;
import org.test.user.StudentMenu;



public class MenuProgram {
	
	//com.mysql.cj.jdbc.Driver.class
	
	
	static Scanner sc = new Scanner(System.in);
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.SignUp");
		System.out.println("2.SignIn");
		System.out.println("3.SignOut");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) throws SQLException, Exception {
		
		// TODO Auto-generated method stub
		
		
		int choice;
		while((choice=MenuProgram.menuList())!=0)
		{
			switch (choice) {
			case 1:
				sign_in();
				break;
			case 2:
				sign_up();	
				
				break;
			case 3:
				break;
			default:
				break;
			}
		}

	

}
	private static void sign_up() {
		// TODO Auto-generated method stub
		
		String name = null,email=null,role=null,password=null;
		int id=0,phone=0;
		System.out.println("Id");
		id=sc.nextInt();
		System.out.println("Name");
		name=sc.next();
		System.out.println("Phone");
		phone=sc.nextInt();
		System.out.println("email");
		email=sc.next();
		System.out.println("role");
		role=sc.next();
		System.out.println("Password");
		password=sc.next();
		
		try( DBUtils dao = new DBUtils()){
			User user = new User(id,name,phone,email,password,role);
			int count = dao.insert( user );
			System.out.println(count+" record(s) Inserted");
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
		
	}
	private static void sign_in() throws SQLException, Exception {
		// TODO Auto-generated method stub
	
		String email = null,password = null;
		System.out.println("Enter Email");
		email=sc.next();
		System.out.println("Enter Password");
		password=sc.next();

		try( Connection connection = DBUtils.getConnection();
				Statement statement = connection.createStatement();)
		{
			
			String sql = "SELECT * FROM user WHERE Email='" + email + "' AND Password='" +password +"'";
			
			try( ResultSet rs = statement.executeQuery(sql)){
				while(rs.next()){
					//Retrieve by column name
					String dbemail  = rs.getString("Email");
					String dbpassword = rs.getString("Password");
					if(email.equals(dbemail) && password.equals(password))
					{
						System.out.println(rs.getString("ROLE").toString()+"hi");
						
						 if(rs.getString("Role").equals("librarian")) librarianMenu.librarianArea();
						  else if(rs.getString("Role").equals("owner")) OwnerMenu.OwnerArea(); 
						  else if(rs.getString("Role").equals("Student")) StudentMenu.StudentArea();
						 	
					}
					else {
						System.out.println("Invalid email, password or role.");
					}
				}
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
 }
}
