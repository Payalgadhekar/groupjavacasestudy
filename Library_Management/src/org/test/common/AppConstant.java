package org.test.common;

public class AppConstant {
	static String  USER_DB="users.db";
	String BOOK_DB	="books.db";
	String BOOKCOPY_DB="bookcopies.db";
	String ISSUERECORD_DB="issuerecord.db";
	String PAYMENT_DB="payment.db";

	String ROLE_OWNER ="owner";
	String ROLE_LIBRARIAN ="librarian";
	String ROLE_MEMBER =	"member";

	String STATUS_AVAIL	="available";
	String STATUS_ISSUED="issued";

	String PAY_TYPE_FEES="fees";
	String PAY_TYPE_FINE="fine";

	int FINE_PER_DAY	=	5;
	int BOOK_RETURN_DAYS=7;
	int MEMBERSHIP_MONTH_DAYS=30;

	String EMAIL_OWNER	="payal@gmail.com";


}